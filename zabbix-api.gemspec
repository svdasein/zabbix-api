# frozen_string_literal: true

require_relative "lib/zabbix/api/version"

Gem::Specification.new do |spec|
  spec.name          = "zabbix-api-simple"
  spec.version       = Zabbix::Api::VERSION
  spec.authors       = ["David Parker"]
  spec.email         = ["daveparker01@gmail.com"]

  spec.summary       = "Zabbix API wrapper"
  spec.description   = "This Zabbix API library strives to be syntactically as similar as possible to the API as described by the Zabbix documentation"
  spec.homepage      = "https://svdasein.gitlab.io/zabbix-api"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.4.0")

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = 'https://gitlab.com/svdasein/zabbix-api'
  spec.metadata["changelog_uri"] = "https://gitlab.com/svdasein/zabbix-api/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
  spec.add_dependency 'faraday', ">= 2.0"
  spec.add_dependency 'faraday-net_http', ">= 2.0"
  spec.add_dependency 'optimist'
  spec.add_dependency 'amazing_print'
  spec.add_dependency 'pry'
end
