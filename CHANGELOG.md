# 0.1.4
- Just doc changes
# 0.1.5
- deps fix for pry
- add known_objects method
- add exception trap for json parse issues w/ note about php memory
- change zapishell.rb to start in the api context
- add exception for apiinfo.version (refuses auth token for some reason)
- add pretty inspection mode to zapishell.rb
# 0.1.6
- force faraday 1.8 - 2.0 appears to break things
# 0.2.0
- changes to support faraday 2.0. Note that faraday < 2.0 is incompatible with 2.0+ - when you install zabbix-api-simple 0.2.0 it will install faraday 2.0
# 0.2.1 & 0.2.2
- another faraday fix
# 0.2.3
- fix for zabbix server >= 6.4.0 api login param change (user => username)
